// 发送异步代码的此数
let ajaxTimes = 0;

export const request = (params) => {
    // 判断url中是否带有/my/
    let header={...params.header};
    if (params.url.includes("/my/")){
        // 拼接header带上token
        header["Authorization"] = wx.getStorageSync('token');
    }

    ajaxTimes++;

    // 显示加载效果
    wx.showLoading({
        title: '玩命加载中',
        mask: true
    })

    let baseUrl = 'http://localhost:8080';
    // 定义公共url
    if (params.url.includes("/index/") || params.url.includes("/categories")){
        baseUrl = 'http://localhost:8080';
    }else{
        baseUrl = 'https://api-hmugo-web.itheima.net/api/public/v1';
    }

    return new Promise((resolve, reject) => {
        wx.request({
            ...params,
            header:header,
            url: baseUrl + params.url,
            success: (result) => {
                // console.log(result);
                // console.log(baseUrl);
                if (baseUrl == 'http://localhost:8080') {
                    resolve(result.data);
                }else{
                    resolve(result.data.message);
                }
            },
            fail: (err) => {
                reject(err);
            },
            complete: function () {
                ajaxTimes--;
                if(ajaxTimes===0){
                    // 关闭等待图标
                    wx.hideLoading();
                }
            }
        });
    })
}