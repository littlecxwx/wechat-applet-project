import regeneratorRuntime from '../../lib/runtime/runtime';
import {
  request
} from '../../request/index.js';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 小导航栏
    tabs:[
      {
        id:0,
        value:"综合",
        isActive:true
      },
      {
        id:1,
        value:"销量",
        isActive:false
      },
      {
        id:2,
        value:"价格",
        isActive:false
      }
    ],
    goodsList:[]
  },

  // 接口需要的参数
  QueryParams:{
    query:"",
    cid:"",
    pagenum:1,
    pagesize:10
  },

  // 总页数
  totalPages:1,

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.QueryParams.cid = options.cid;
    this.getGoodList();
  },

  // 获取商品列表
  async getGoodList(){
    const res = await request({url:"/goods/search", data:this.QueryParams});
    // console.log(res);

    // 获取总条数
    const total = res.total

    // 计算总页数
    this.totalPages = Math.ceil(total/this.QueryParams.pagesize)
    console.log(this.totalPages);

    this.setData({
      // 拼接数据
      goodsList:[...this.data.goodsList, ...res.goods]
    })

    // 关闭下拉刷新窗口
    wx.stopPullDownRefresh({
      success: (res) => {},
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  // 标题点击事件
  handleTabsItemChange(e){
    // console.log(e);

    // 获取被点击的标题索引
    const {index} = e.detail;

    // 修改源数组
    let {tabs} = this.data;
    tabs.forEach((v, i)=>i===index?v.isActive=true:v.isActive=false);

    // 赋值到data中
    this.setData({
      tabs
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   * 下拉刷新事件
   */
  onPullDownRefresh: function () {
    // console.log("refresh");
    // 重置数组
    this.setData({
      goodsList: []
    })

    // 重置页码
    this.QueryParams.pagenum=1;

    // 重新发送请求
    this.getGoodList();
  },

  /**
   * 页面上拉触底事件的处理函数
   * 下拉刷新
   */
  onReachBottom: function () {
    // console.log("页面触底");
    if(this.QueryParams.pagenum>=this.totalPages){
      // console.log("没有下一页数据");
      wx.showToast({
        title: '没有下一页数据',
        icon: 'none',
        image: '',
        duration: 1500,
        mask: false,
        success: (result)=>{
          
        },
        fail: ()=>{},
        complete: ()=>{}
      });
    }else{
      // 有下一页
      // console.log("有下一页");
      this.QueryParams.pagenum++;
      this.getGoodList();
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})