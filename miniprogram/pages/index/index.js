// // index.js
// // const app = getApp()
// const { envList } = require('../../envList.js');

// Page({
//   data: {
//     showUploadTip: false,
//     powerList: [{
//       title: '云函数',
//       tip: '安全、免鉴权运行业务代码',
//       showItem: false,
//       item: [{
//         title: '获取OpenId',
//         page: 'getOpenId'
//       },
//       //  {
//       //   title: '微信支付'
//       // },
//        {
//         title: '生成小程序码',
//         page: 'getMiniProgramCode'
//       },
//       // {
//       //   title: '发送订阅消息',
//       // }
//     ]
//     }, {
//       title: '数据库',
//       tip: '安全稳定的文档型数据库',
//       showItem: false,
//       item: [{
//         title: '创建集合',
//         page: 'createCollection'
//       }, {
//         title: '更新记录',
//         page: 'updateRecord'
//       }, {
//         title: '查询记录',
//         page: 'selectRecord'
//       }, {
//         title: '聚合操作',
//         page: 'sumRecord'
//       }]
//     }, {
//       title: '云存储',
//       tip: '自带CDN加速文件存储',
//       showItem: false,
//       item: [{
//         title: '上传文件',
//         page: 'uploadFile'
//       }]
//     }, {
//       title: '云托管',
//       tip: '不限语言的全托管容器服务',
//       showItem: false,
//       item: [{
//         title: '部署服务',
//         page: 'deployService'
//       }]
//     }],
//     envList,
//     selectedEnv: envList[0],
//     haveCreateCollection: false
//   },

//   onClickPowerInfo(e) {
//     const index = e.currentTarget.dataset.index;
//     const powerList = this.data.powerList;
//     powerList[index].showItem = !powerList[index].showItem;
//     if (powerList[index].title === '数据库' && !this.data.haveCreateCollection) {
//       this.onClickDatabase(powerList);
//     } else {
//       this.setData({
//         powerList
//       });
//     }
//   },

//   onChangeShowEnvChoose() {
//     wx.showActionSheet({
//       itemList: this.data.envList.map(i => i.alias),
//       success: (res) => {
//         this.onChangeSelectedEnv(res.tapIndex);
//       },
//       fail (res) {
//         console.log(res.errMsg);
//       }
//     });
//   },

//   onChangeSelectedEnv(index) {
//     if (this.data.selectedEnv.envId === this.data.envList[index].envId) {
//       return;
//     }
//     const powerList = this.data.powerList;
//     powerList.forEach(i => {
//       i.showItem = false;
//     });
//     this.setData({
//       selectedEnv: this.data.envList[index],
//       powerList,
//       haveCreateCollection: false
//     });
//   },

//   jumpPage(e) {
//     wx.navigateTo({
//       url: `/pages/${e.currentTarget.dataset.page}/index?envId=${this.data.selectedEnv.envId}`,
//     });
//   },

//   onClickDatabase(powerList) {
//     wx.showLoading({
//       title: '',
//     });
//     wx.cloud.callFunction({
//       name: 'quickstartFunctions',
//       config: {
//         env: this.data.selectedEnv.envId
//       },
//       data: {
//         type: 'createCollection'
//       }
//     }).then((resp) => {
//       if (resp.result.success) {
//         this.setData({
//           haveCreateCollection: true
//         });
//       }
//       this.setData({
//         powerList
//       });
//       wx.hideLoading();
//     }).catch((e) => {
//       console.log(e);
//       this.setData({
//         showUploadTip: true
//       });
//       wx.hideLoading();
//     });
//   }
// });

  const db = wx.cloud.database();
  // const indexSwiperImagesCollection = db.collection('indexSwiperImages');
  import {
    request
  } from '../../request/index.js';
Page({
  data: {
    // // 轮播图数组
    // swiperList: [{
    //     image_src: "https://gtms04.alicdn.com/tps/i4/TB1pgxYJXXXXXcAXpXXrVZt0FXX-640-200.jpg",
    //     goods_id: 0,
    //   },
    //   {
    //     image_src: "https://img.alicdn.com/imgextra/i2/6000000006881/O1CN01J0ixfi20hY4eksELF_!!6000000006881-0-lubanimage.jpg",
    //     goods_id: 1,
    //   },
    //   {
    //     image_src: "https://img.alicdn.com/imgextra/i4/6000000002596/O1CN01T5Gkg91V30viNbpvu_!!6000000002596-0-lubanimage.jpg",
    //     goods_id: 2,
    //   }
    // ],
    // // 导航栏数组
    // catesList: [{
    //     image_src: "https://gw.alicdn.com/tfs/TB1LvIxVAvoK1RjSZFDXXXY3pXa-183-144.png",
    //     name: "今日爆款"
    //   },
    //   {
    //     image_src: "https://gw.alicdn.com/tfs/TB19uWKXkCy2eVjSZPfXXbdgpXa-183-144.png",
    //     name: "天猫国际"
    //   },
    //   {
    //     image_src: "https://gw.alicdn.com/tfs/TB1OIxTcLc3T1VjSZLeXXbZsVXa-183-144.png",
    //     name: "天猫新品"
    //   },
    //   {
    //     image_src: "https://gw.alicdn.com/tfs/TB1FucwVwHqK1RjSZFgXXa7JXXa-183-144.png",
    //     name: "天猫超市"
    //   },
    //   {
    //     image_src: "https://gw.alicdn.com/tfs/TB1nBktVxTpK1RjSZR0XXbEwXXa-183-144.png",
    //     name: "分类"
    //   }
    // ],
    // // 楼哦层数组
    // floorList: [{
    //     "floor_title": {
    //       "name": "时尚女装",
    //       "image_src": "https://s1.ax1x.com/2022/03/20/qeQofH.png"
    //     },
    //     "product_list": [{
    //         "name": "优质服饰",
    //         "image_src": "https://s1.ax1x.com/2022/03/20/qeleNF.png",
    //         "image_width": "232",
    //         "open_type": "navigate",
    //         "navigator_url": "/pages/goods_list/index?query=服饰"
    //       },
    //       {
    //         "name": "春季热门",
    //         "image_src": "https://s1.ax1x.com/2022/03/20/qelU9H.png",
    //         "image_width": "233",
    //         "open_type": "navigate",
    //         "navigator_url": "/pages/goods_list/index?query=热"
    //       },
    //       {
    //         "name": "爆款清仓",
    //         "image_src": "https://s1.ax1x.com/2022/03/20/qelrHf.png",
    //         "image_width": "233",
    //         "open_type": "navigate",
    //         "navigator_url": "/pages/goods_list/index?query=爆款"
    //       },
    //       {
    //         "name": "倒春寒",
    //         "image_src": "https://s1.ax1x.com/2022/03/20/qelR3j.png",
    //         "image_width": "233",
    //         "open_type": "navigate",
    //         "navigator_url": "/pages/goods_list/index?query=春季"
    //       },
    //       {
    //         "name": "怦然心动",
    //         "image_src": "https://s1.ax1x.com/2022/03/20/qel5D0.png",
    //         "image_width": "233",
    //         "open_type": "navigate",
    //         "navigator_url": "/pages/goods_list/index?query=心动"
    //       }
    //     ]
    //   },
    //   {
    //     "floor_title": {
    //       "name": "户外活动",
    //       "image_src": "https://s1.ax1x.com/2022/03/20/qe19UO.png"
    //     },
    //     "product_list": [{
    //         "name": "勇往直前",
    //         "image_src": "https://s1.ax1x.com/2022/03/20/qe1k2d.png",
    //         "image_width": "232",
    //         "open_type": "navigate",
    //         "navigator_url": "/pages/goods_list/index?query=户外"
    //       },
    //       {
    //         "name": "户外登山包",
    //         "image_src": "https://s1.ax1x.com/2022/03/20/qe1F8H.png",
    //         "image_width": "273",
    //         "open_type": "navigate",
    //         "navigator_url": "/pages/goods_list/index?query=登山包"
    //       },
    //       {
    //         "name": "超强手套",
    //         "image_src": "https://s1.ax1x.com/2022/03/20/qe1AxA.png",
    //         "image_width": "193",
    //         "open_type": "navigate",
    //         "navigator_url": "/pages/goods_list/index?query=手套"
    //       },
    //       {
    //         "name": "户外运动鞋",
    //         "image_src": "https://s1.ax1x.com/2022/03/20/qe1C5D.png",
    //         "image_width": "193",
    //         "open_type": "navigate",
    //         "navigator_url": "/pages/goods_list/index?query=运动鞋"
    //       },
    //       {
    //         "name": "冲锋衣系列",
    //         "image_src": "https://s1.ax1x.com/2022/03/20/qe1iPe.png",
    //         "image_width": "273",
    //         "open_type": "navigate",
    //         "navigator_url": "/pages/goods_list/index?query=冲锋衣"
    //       }
    //     ]
    //   },
    //   {
    //     "floor_title": {
    //       "name": "箱包配饰",
    //       "image_src": "https://s1.ax1x.com/2022/03/20/qe3VW4.png"
    //     },
    //     "product_list": [{
    //         "name": "清新气质",
    //         "image_src": "https://s1.ax1x.com/2022/03/20/qe3uO1.png",
    //         "image_width": "232",
    //         "open_type": "navigate",
    //         "navigator_url": "/pages/goods_list?query=饰品"
    //       },
    //       {
    //         "name": "复古胸针",
    //         "image_src": "https://s1.ax1x.com/2022/03/20/qe3Mex.png",
    //         "image_width": "263",
    //         "open_type": "navigate",
    //         "navigator_url": "/pages/goods_list?query=胸针"
    //       },
    //       {
    //         "name": "韩版手链",
    //         "image_src": "https://s1.ax1x.com/2022/03/20/qe3eSJ.png",
    //         "image_width": "203",
    //         "open_type": "navigate",
    //         "navigator_url": "/pages/goods_list?query=手链"
    //       },
    //       {
    //         "name": "水晶项链",
    //         "image_src": "https://s1.ax1x.com/2022/03/20/qe3ml9.png",
    //         "image_width": "193",
    //         "open_type": "navigate",
    //         "navigator_url": "/pages/goods_list?query=水晶项链"
    //       },
    //       {
    //         "name": "情侣表",
    //         "image_src": "https://s1.ax1x.com/2022/03/20/qe3nyR.png",
    //         "image_width": "273",
    //         "open_type": "navigate",
    //         "navigator_url": "/pages/goods_list?query=情侣表"
    //       }
    //     ],
    //     indexSwiperImages: []
    //   }
    // ],
    swiperList: [],
    catesList: [],
    floorList: []
  },

  async getIndex(){
    const swiperList = await request({url:"/index/swiperList"});
    const catesList = await request({url:"/index/catesList"});
    const floorList = await request({url:"/index/floorList"});
    this.setData({
      swiperList,
      floorList,
      catesList
    })
  },
  onLoad: function (options) {
    //Do some initialize when page load.

  },
  onReady: function () {
    //Do some when page ready.

  },
  onShow: function () {
    //Do some when page show.
    // indexSwiperImagesCollection.get().then(res => {
    //   console.log("成功读取轮播图", res.data)
    //   this.setData({
    //     indexSwiperImages: res.data
    //   })
    // })

    this.getIndex();
  },
  onHide: function () {
    //Do some when page hide.

  },
  onUnload: function () {
    //Do some when page unload.

  },
  onPullDownRefresh: function () {
    //Do some when page pull down.

  }
})