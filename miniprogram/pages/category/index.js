// pages/category/index.js
const db = wx.cloud.database();
const categoryListCollection = db.collection('categoryList');
import regeneratorRuntime from '../../lib/runtime/runtime';
import {
  request
} from '../../request/index.js';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 左侧菜单数据
    leftMenuList: [],
    // 右侧商品数据
    rightContent: [],
    // 备点即的左侧菜单
    currentIndex: 0,
    // 右侧内容滚动条距离顶部的距离
    scrollTop: 0
  },

  // 接口返回的数据
  Cates: [],

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // this.getCates();
    // 判断本地存储有没有旧的数据，如果没有旧的数据就发送新的情求，有旧的数据就使用本地存储数据
    const Cates = wx.getStorageSync('cates');
    if(!Cates){
      this.getCates();
    }else{
      // 有旧的数据
      if(Date.now()-Cates.time>1000){
        this.getCates();
      }else{
        // 使用旧的数据
        // console.log(Cates.data[0]);
        this.cates = Cates.data;

        // 构造左侧菜单数据
        let leftMenuList=Cates.data.map(v=>v.cat_name);
        // console.log(leftMenuList);

        // 构造右侧菜单数据
        let rightContent=Cates.data[0].children;

        this.setData({
          leftMenuList,
          rightContent
        })
      }
    }
  },

  // 获取分类数据
  // getCates() {
  //   request({
  //       url: "/categories"
  //     })
  //     .then(res => {
  //       this.Cates=res.data.message;

  //       // 构造左侧菜单数据
  //       let leftMenuList=this.Cates.map(v=>v.cat_name);

  //       // 构造右侧菜单数据
  //       let rightContent=this.Cates[0].children;

  //       this.setData({
  //         leftMenuList,
  //         rightContent
  //       })
  //     })
  //   // categoryListCollection.get().then(res => {
  //   //   console.log("成功读取商品类别", res.data)
  //   //   this.Cates = res.data[0].message;

  //   //   // 构造左侧菜单数据
  //   //   let leftMenuList = this.Cates.map(v => v.cat_name);

  //   //   // 构造右侧菜单数据
  //   //   let rightContent = this.Cates[0].children;

  //   //   this.setData({
  //   //     leftMenuList,
  //   //     rightContent
  //   //   })
  //   // })
  // },

  // 获取分类数据
  async getCates() {
    const res = await request({url:"/categories"});
    // this.Cates=res.data.message;
    this.Cates=res;

    // 数据存储到本地
    wx.setStorageSync('cates', {time:Date.now(),data:this.Cates});

    // 构造左侧菜单数据
    let leftMenuList=this.Cates.map(v=>v.cat_name);
    // console.log(leftMenuList);

    // 构造右侧菜单数据
    let rightContent=this.Cates[0].children;

    this.setData({
      leftMenuList,
      rightContent
    })
  },

  // 左侧菜单的点击事件
  handleItemTap(e) {
    const {
      index
    } = e.currentTarget.dataset;

    let rightContent = this.Cates[index].children;
    this.setData({
      currentIndex: index,
      rightContent,
      scrollTop: 0
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // this.getCates();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})