import {
  getSetting,
  chooseAddress,
  openSetting,
  showModal,
  showToast
} from "../../utils/asyncWx.js";
import regeneratorRuntime from '../../lib/runtime/runtime';

Page({
  /**
   * 页面的初始数据
   */
  data: {
    address: {},
    cart: [],
    allChecked: false,
    totalPrice: 0,
    totalNum: 0
  },

  // 点击收货地址事件
  async handleChooseAddress() {
    // console.log("ChooseAddress");
    // wx.chooseAddress({
    //   success: (result) => {
    //     console.log(result);
    //   },
    //   fail: (res) => {},
    //   complete: (res) => {},
    // })

    // wx.getSetting({
    //   withSubscriptions: true,
    //   success: (result) => {
    //     // console.log(result);
    //     // 获取权限状态
    //     const scopeAddress = result.authSetting["scope.address"];
    //     // 判断权限状态
    //     if (scopeAddress===true || scopeAddress===undefined){
    //       wx.chooseAddress({
    //         success: (result1) => {
    //           console.log(result1);
    //         },
    //         fail: (res) => {},
    //         complete: (res) => {},
    //       })
    //     }else{
    //       wx.openSetting({
    //         withSubscriptions: true,
    //         success: (result2) => {
    //           wx.chooseAddress({
    //             success: (result3) => {
    //               console.log(result3);
    //             },
    //             fail: (res) => {},
    //             complete: (res) => {},
    //           })
    //         },
    //         fail: (res) => {},
    //         complete: (res) => {},
    //       })
    //     }
    //   },
    //   fail: (res) => {},
    //   complete: (res) => {},
    // })

    try {
      const res1 = await getSetting();
      const scopeAddress = res1.authSetting["scope.address"];
      if (scopeAddress === false) {
        await openSetting();
      }
      let address = await chooseAddress();
      // console.log(res2);

      address.all = address.provinceName + address.cityName + address.countyName + address.detailInfo;

      // 存入到缓存中
      wx.setStorageSync('address', address)
    } catch (error) {
      console.log(error);
    }
  },

  // 商品的选中
  handleItemChange(e) {
    // 获取商品id
    const goods_id = e.currentTarget.dataset.id;

    // 获取购物车数组
    let {
      cart
    } = this.data;

    // 获取需要被修改的对象
    let index = cart.findIndex(v => v.goods_id === goods_id);

    // 选中状态取反
    cart[index].checked = !cart[index].checked;

    this.setCart(cart);
  },

  // 购物车功能设置
  setCart(cart) {
    // 计算全选
    // const allChecked = cart.length?cart.every(v=>v.checked):false;
    let allChecked = true;

    // 总价格&总数量
    let totalPrice = 0;
    let totalNum = 0;
    cart.forEach(v => {
      if (v.checked) {
        totalPrice += v.num * v.goods_price;
        totalNum += v.num;
      } else {
        allChecked = false;
      }
    })

    // 判断数组是否为空
    allChecked = cart.length != 0 ? allChecked : false;

    this.setData({
      cart,
      totalPrice,
      totalNum,
      allChecked
    })
    wx.setStorageSync('cart', cart);

  },

  // 全选&反全选
  handleItemAllCheck() {
    // 获取data数据
    let {
      cart,
      allChecked
    } = this.data;

    // 修改值
    allChecked = !allChecked;

    // 循环修改cart数组
    cart.forEach(v => v.checked = allChecked);

    // 存储到data和缓存中
    this.setCart(cart);
  },

  // 商品数量编辑
  async handleItemNumEdit(e) {
    // 获取传递的参数
    const {
      operation,
      id
    } = e.currentTarget.dataset;
    // console.log(operation, id);

    // 获取购物车数组
    let {
      cart
    } = this.data;

    // 获取需要修改的商品索引
    const index = cart.findIndex(v => v.goods_id === id);

    // 判断是否要删除
    if (cart[index].num === 1 && operation === -1) {
      // 弹窗提示
      // wx.showModal({
      //   title: '提示',
      //   content: '您是否要删除该商品',
      //   showCancel: true,
      //   cancelText: '取消',
      //   cancelColor: '#000000',
      //   confirmText: '确定',
      //   confirmColor: '#3CC51F',
      //   success: (result) => {
      //     if (result.confirm) {
      //       cart.splice(index, 1);
      //       this.setCart(cart);
      //     } else {

      //     }
      //   },
      //   fail: () => {},
      //   complete: () => {}
      // });
      const res = await showModal({
        content: "您是否要删除该商品"
      })
      if (res.confirm) {
        cart.splice(index, 1);
        this.setCart(cart);
      }
    } else {
      // 修改数量
      cart[index].num += operation;

      // 设置缓存和data
      this.setCart(cart);
    }
  },

  async handlePay(){
    // 判断收货地址
    const {address, totalNum} = this.data;
    if(!address){
      await showToast({title:"您还没选择收货地址"});
      return;
    }

    // 判断用户有无选中商品
    if(totalNum===0){
      await showToast({title:"您还未选中商品"});
      return;
    }

    // 跳转支付页面
    wx.navigateTo({
      url: '../pay/index',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 获取缓存中的收货地址信息
    const address = wx.getStorageSync('address');
    // 获取缓存中的购物车信息
    const cart = wx.getStorageSync('cart') || [];

    this.setData({
      address
    });
    this.setCart(cart);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})