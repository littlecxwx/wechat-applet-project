import regeneratorRuntime from '../../lib/runtime/runtime';
import {
  request
} from '../../request/index.js';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    goodsObj:{},
    isCollect: false
  },

  // 商品对象
  GoodsInfo:{},

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const {goods_id} = options;
    // console.log(goods_id);
    this.getGoodsDetail(goods_id);
  },

  // 获取商品详情数据
  async getGoodsDetail(goods_id){
    const goodsObj = await request({url:"/goods/detail",data:{goods_id}});
    // console.log(res);
    this.GoodsInfo = goodsObj;
    // 获取缓存中的商品收藏
    let collect = wx.getStorageSync("collect")||[];
    let isCollect = collect.some(v=>v.goods_id===this.GoodsInfo.goods_id);
    this.setData({
      goodsObj:{
        goods_name:goodsObj.goods_name,
        goods_price:goodsObj.goods_price,
        goods_introduce:goodsObj.goods_introduce.replace(/\.webp/g,'.jpg'),
        pics:goodsObj.pics
      },
      isCollect
    })
  },

  // 点击轮播图放大预览
  handlePreviewImage(e){
    // console.log("预览");
    const urls = this.GoodsInfo.pics.map(v=>v.pics_mid);
    // 接受图片传递过来的url
    const current = e.currentTarget.dataset.url;
    wx.previewImage({
      urls,
      current,
      showmenu: true,
      success: (res) => {},
      fail: (res) => {},
      complete: (res) => {},
    })
  },

  // 点击加入购物车
  handleCartAdd(){
    // console.log("加入购物车");
    // 获取缓存中的购物车
    let cart = wx.getStorageSync('cart')||[];

    // 判断商品对象是否存在购物车中
    let index = cart.findIndex(v=>v.goods_id===this.GoodsInfo.goods_id);
    if(index===-1){
      // 第一次添加
      this.GoodsInfo.num=1;
      this.GoodsInfo.checked=false;
      cart.push(this.GoodsInfo);
    }else{
      // 购物车已经存在该商品
      cart[index].num++;
    }

    // 存入缓存
    wx.setStorageSync('cart', cart);

    // 弹窗提示
    wx.showToast({
      title: '加入成功',
      icon: 'success',
      // 防止用户点击按钮过快
      mask: true
    })
  },

  // 点击商品收藏事件
  handleCollect(){
    let isCollect = false;

    // 获取缓存中的商品收藏数组
    let collect = wx.getStorageSync('collect')||[];

    // 判断商品是否被收藏过
    let index = collect.findIndex(v=>v.goods_id===this.GoodsInfo.goods_id);

    // 已收藏
    if(index!==-1){
      collect.splice(index, 1);
      isCollect=false;
      wx.showToast({
        title: '取消成功',
        icon: 'success',
        mask: true,
        success: (res) => {},
        fail: (res) => {},
        complete: (res) => {},
      })
    }else{
      collect.push(this.GoodsInfo);
      isCollect=true;
      wx.showToast({
        title: '收藏成功',
        icon: 'success',
        mask: true,
        success: (res) => {},
        fail: (res) => {},
        complete: (res) => {},
      })
    }

    // 数组存储到缓存中
    wx.setStorageSync("collect",collect);

    // 修改data数据
    this.setData({
      isCollect
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let pages = getCurrentPages();
    let currentPage = pages[pages.length-1];
    let options = currentPage.options;
    
    const {goods_id} = options;
    // console.log(goods_id);
    this.getGoodsDetail(goods_id);

    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})