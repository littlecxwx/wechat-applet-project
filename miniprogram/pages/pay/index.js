import {
  getSetting,
  chooseAddress,
  openSetting,
  showModal,
  showToast,
  requestPayment
} from "../../utils/asyncWx.js";
import {
  request
} from "../../request/index.js";
import regeneratorRuntime from '../../lib/runtime/runtime';

Page({
  /**
   * 页面的初始数据
   */
  data: {
    address: {},
    cart: [],
    totalPrice: 0,
    totalNum: 0
  },



  // 购物车功能设置
  setCart(cart) {


  },



  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 获取缓存中的收货地址信息
    const address = wx.getStorageSync('address');
    // 获取缓存中的购物车信息
    let cart = wx.getStorageSync('cart') || [];

    // 过滤后的购物车数组
    cart = cart.filter(v => v.checked);

    this.setData({
      address
    });

    // 总价格&总数量
    let totalPrice = 0;
    let totalNum = 0;
    cart.forEach(v => {
      totalPrice += v.num * v.goods_price;
      totalNum += v.num;
    })


    this.setData({
      cart,
      totalPrice,
      totalNum,
      address
    })
  },

  // 点击支付事件
  async handleOrderPay() {
    try {
      // 判断缓存中有无token
      const token = wx.getStorageSync("token");
      if (!token) {
        wx.navigateTo({
          url: '../auth/index',
        });
        return;
      }
      // console.log("token已存在");

      // 创建订单
      // --请求头参数
      // const header = {
      //   Authorization: token
      // };
      // --准备请求体参数
      const order_price = this.data.totalPrice;
      const consignee_addr = this.data.address.all;
      const cart = this.data.cart;
      let goods = [];
      cart.forEach(v => goods.push({
        goods_id: v.goods_id,
        goods_number: v.goods_number,
        goods_price: v.goods_price
      }))
      const orderParams = {
        order_price,
        consignee_addr,
        goods
      };

      // 准备发送请求 获取订单编号
      const {
        order_number
      } = await request({
        url: "/my/orders/create",
        method: "POST",
        data: orderParams
      });
      // console.log(order_number);

      // 发起预支付
      const {
        pay
      } = await request({
        url: "/my/orders/req_unifiedorder",
        method: "POST",
        data: {
          order_number
        }
      });
      // console.log(pay);

      // 发起微信支付
      await requestPayment(pay);

      // 查询后台订单状态
      const res = await request({
        url: "/my/orders/chkOrder",
        method: "POST",
        data: {
          order_number
        }
      });
      await showToast({
        title: "支付成功"
      });

      let newCart = this.data.cart;
      newCart = newCart.filter(v=>!v.checked);
      wx.setStorageSync("cart", newCart);

      // 跳转订单页面
      wx.navigateTo({
        url: '../order/index',
        events: events,
        success: (result) => {},
        fail: (res) => {},
        complete: (res) => {},
      })
    } catch (error) {
      await showToast({
        title: "支付失败"
      });
      console.log(error);
    }

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})