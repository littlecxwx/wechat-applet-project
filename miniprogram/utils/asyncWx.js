export const getSetting = () => {
    return new Promise((resolve, reject) => {
        wx.getSetting({
            withSubscriptions: true,
            success: (result) => {
                resolve(result);
            },
            fail: (err) => {
                reject(err);
            },
            complete: (res) => {},
        })
    })
}

export const chooseAddress = () => {
    return new Promise((resolve, reject) => {
        wx.chooseAddress({
            withSubscriptions: true,
            success: (result) => {
                resolve(result);
            },
            fail: (err) => {
                reject(err);
            },
            complete: (res) => {},
        })
    })
}

export const openSetting = () => {
    return new Promise((resolve, reject) => {
        wx.openSetting({
            withSubscriptions: true,
            success: (result) => {
                resolve(result);
            },
            fail: (err) => {
                reject(err);
            },
            complete: (res) => {},
        })
    })
}

/**
 * @Param {object} param 参数
 */
export const showModal = ({content}) => {
    return new Promise((resolve, reject) => {
        wx.showModal({
            title: '提示',
            content: content,
            showCancel: true,
            cancelText: '取消',
            cancelColor: '#000000',
            confirmText: '确定',
            confirmColor: '#3CC51F',
            success: (result) => {
                resolve(result);
            },
            fail: (err) => {
                reject(err);
            },
            complete: () => {}
        });
    })
}

/**
 * @Param {object} param 参数
 */
export const showToast = ({title}) => {
    return new Promise((resolve, reject) => {
        wx.showToast({
          title: title,
          duration: 2000,
          icon: 'none',
          mask: true,
          success: (res) => {
              resolve(res);
          },
          fail: (err) => {
              reject(err);
          },
          complete: (res) => {},
        })
    })
}

export const login = () => {
    return new Promise((resolve, reject) => {
        wx.login({
            timeout:10000,
            success: (result)=>{
                resolve(result);
            },
            fail: (err)=>{
                reject(err);
            },
            complete: ()=>{}
        });
    })
}

/**
 * @Param {object} param 支付所必要的参数
 */
export const requestPayment = ({pay}) => {
    return new Promise((resolve, reject) => {
        wx.requestPayment({
            ...pay,
            success: (result)=>{
                resolve(result);
            },
            fail: (err)=>{
                reject(err);
            },
            complete: ()=>{}
        });
    })
}