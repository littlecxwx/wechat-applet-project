package com.taobao.demo.repository;

import com.taobao.demo.entity.SwiperList;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SwiperListRepository extends MongoRepository<SwiperList, String> {
}
