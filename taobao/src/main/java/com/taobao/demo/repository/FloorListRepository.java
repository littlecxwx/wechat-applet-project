package com.taobao.demo.repository;

import com.taobao.demo.entity.FloorList;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FloorListRepository extends MongoRepository<FloorList, String> {
}
