package com.taobao.demo.repository;

import com.taobao.demo.entity.CatesList;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CatesListRepository extends MongoRepository<CatesList, String> {
}
