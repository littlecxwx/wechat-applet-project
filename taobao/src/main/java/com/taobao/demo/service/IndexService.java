package com.taobao.demo.service;

import com.taobao.demo.entity.CatesList;
import com.taobao.demo.entity.FloorList;
import com.taobao.demo.entity.SwiperList;

import java.util.List;

public interface IndexService {
    List<CatesList> getCatesList();

    List<SwiperList> getSwiperList();

    List<FloorList> getFloorList();
}
