package com.taobao.demo.service;

import com.taobao.demo.entity.Categories;

import java.util.List;

public interface CategoriesService {
    List<Categories> getInfo();
}
