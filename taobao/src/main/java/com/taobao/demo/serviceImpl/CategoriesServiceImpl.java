package com.taobao.demo.serviceImpl;

import com.taobao.demo.entity.Categories;
import com.taobao.demo.repository.CategoriesRepository;
import com.taobao.demo.service.CategoriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoriesServiceImpl implements CategoriesService {
    @Autowired
    private CategoriesRepository categoriesRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public List<Categories> getInfo() {
        return categoriesRepository.findAll();
    }
}
