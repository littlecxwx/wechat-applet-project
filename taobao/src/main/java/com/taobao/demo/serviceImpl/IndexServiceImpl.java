package com.taobao.demo.serviceImpl;

import com.taobao.demo.entity.CatesList;
import com.taobao.demo.entity.FloorList;
import com.taobao.demo.entity.SwiperList;
import com.taobao.demo.repository.CatesListRepository;
import com.taobao.demo.repository.FloorListRepository;
import com.taobao.demo.repository.SwiperListRepository;
import com.taobao.demo.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IndexServiceImpl implements IndexService {
    @Autowired
    private CatesListRepository catesListRepository;

    @Autowired
    private SwiperListRepository swiperListRepository;

    @Autowired
    private FloorListRepository floorListRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    public IndexServiceImpl() {
    }

    @Override
    public List<CatesList> getCatesList() {
        return catesListRepository.findAll();
    }

    @Override
    public List<SwiperList> getSwiperList() {
        return swiperListRepository.findAll();
    }

    @Override
    public List<FloorList> getFloorList() {
        return floorListRepository.findAll();
    }
}
