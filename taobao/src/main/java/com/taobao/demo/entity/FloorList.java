package com.taobao.demo.entity;

import java.util.List;

public class FloorList {
    Object floor_title;
    String name;
    String image_src;
    List product_list;
    String image_width;
    String open_type;
    String navigator_url;

    public Object getFloor_title() {
        return floor_title;
    }

    public void setFloor_title(Object floor_title) {
        this.floor_title = floor_title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage_src() {
        return image_src;
    }

    public void setImage_src(String image_src) {
        this.image_src = image_src;
    }

    public List getProduct_list() {
        return product_list;
    }

    public void setProduct_list(List product_list) {
        this.product_list = product_list;
    }

    public String getImage_width() {
        return image_width;
    }

    public void setImage_width(String image_width) {
        this.image_width = image_width;
    }

    public String getOpen_type() {
        return open_type;
    }

    public void setOpen_type(String open_type) {
        this.open_type = open_type;
    }

    public String getNavigator_url() {
        return navigator_url;
    }

    public void setNavigator_url(String navigator_url) {
        this.navigator_url = navigator_url;
    }
}
