package com.taobao.demo.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;

@Document(collection = "categories")
public class Categories {
    int cat_id;
    String cat_name;
    int cat_pid;
    int cat_level;
    boolean cat_deleted;
    String cat_icon;
    List children;

    public int getCat_id() {
        return cat_id;
    }

    public void setCat_id(int cat_id) {
        this.cat_id = cat_id;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public int getCat_pid() {
        return cat_pid;
    }

    public void setCat_pid(int cat_pid) {
        this.cat_pid = cat_pid;
    }

    public int getCat_level() {
        return cat_level;
    }

    public void setCat_level(int cat_level) {
        this.cat_level = cat_level;
    }

    public boolean isCat_deleted() {
        return cat_deleted;
    }

    public void setCat_deleted(boolean cat_deleted) {
        this.cat_deleted = cat_deleted;
    }

    public String getCat_icon() {
        return cat_icon;
    }

    public void setCat_icon(String cat_icon) {
        this.cat_icon = cat_icon;
    }

    public List getChildren() {
        return children;
    }

    public void setChildren(List children) {
        this.children = children;
    }
}
