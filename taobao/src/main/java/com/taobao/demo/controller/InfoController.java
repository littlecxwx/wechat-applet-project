package com.taobao.demo.controller;

import com.taobao.demo.entity.Categories;
import com.taobao.demo.entity.CatesList;
import com.taobao.demo.service.CategoriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Controller
@RestController
@RequestMapping(path = "/categories")
public class InfoController {
    CategoriesService categoriesService;

    @Autowired
    public InfoController(CategoriesService categoriesService){
        this.categoriesService = categoriesService;
    }

    @GetMapping
    public List<Categories> getInfo(){
        return categoriesService.getInfo();
    }
}
