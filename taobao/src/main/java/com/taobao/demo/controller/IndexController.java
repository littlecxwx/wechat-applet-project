package com.taobao.demo.controller;

import com.taobao.demo.entity.CatesList;
import com.taobao.demo.entity.FloorList;
import com.taobao.demo.entity.SwiperList;
import com.taobao.demo.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Controller
@RestController
public class IndexController {
    IndexService indexService;

    @Autowired
    public IndexController(IndexService indexService){
        this.indexService = indexService;
    }

    @GetMapping("/index/catesList")
    public List<CatesList> getcatesList(){
        return indexService.getCatesList();
    }

    @GetMapping("/index/swiperList")
    public List<SwiperList> getswiperList(){
        return indexService.getSwiperList();
    }

    @GetMapping("/index/floorList")
    public List<FloorList> getfloorList(){
        return indexService.getFloorList();
    }
}
