package com.taobao.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .enable(true)
                .groupName("LittleC")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.example.demo.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        Contact contact = new Contact(
                "LittleC", "https://www.cnblogs.com/LittleCxwx/", "2418643362@qq.com");
        return new ApiInfo(
                "LittleC's api",
                "Springboot + Vue Project -- NewsDemo",
                "v0.1",
                "2418643362@qq.com",    //服务器条款链接
                contact,
                "Apache 2.0",   //许可证
                "http://www.apache.org/licenses/LICENSE-2.0",   //许可证链接
                new ArrayList<>()
        );
//        return new ApiInfoBuilder()
//                .title("New Demo API Doc")
//                .description("Springboot + Vue Project -- NewsDemo")
//                .version("v0.1")
//                .build();
    }
}
